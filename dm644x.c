/*
 * dm644x.c - DM644x specific platform initialization
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "davinci.h"
#include "util.h"

#define VTPIOCR_EN_MASK       (1 << 13)
#define VTPIOCR_RECAL_MASK    (1 << 15)

#define DDRVTPER_EN_MASK      (1 << 0)

#define DDRVTPR_CAL_DATA_MASK 0x03FF

/* List of modules to enable in the PSC */
const int8_t lpsc_en_list[] = {
	LPSC_VPSS_MAST,
	LPSC_VPSS_SLV,
	LPSC_EDMACC,
	LPSC_EDMATC0,
	LPSC_EDMATC1,
	LPSC_EMAC,
	LPSC_EMAC_MEM_CTL,
	LPSC_MDIO,
	LPSC_USB,
	LPSC_ATA,
	LPSC_VLYNQ,
	LPSC_HPI,
	LPSC_DDR2,
	LPSC_AEMIF,
	LPSC_MMC_SD0,
	LPSC_ASP0,
	LPSC_I2C,
	LPSC_UART0,
	LPSC_GPIO,
	LPSC_TIMER0,
	LPSC_ARM,
	LPSC_IMCOP,
};

const size_t lpsc_en_list_len = sizeof(lpsc_en_list) /
	sizeof(lpsc_en_list[0]);

/* List of modules for which to control EMURSTIE */
const int8_t lpsc_emurstie_list[] = {
	LPSC_VPSS_SLV,
	LPSC_EMAC,
	LPSC_EMAC_MEM_CTL,
	LPSC_MDIO,
	LPSC_USB,
	LPSC_ATA,
	LPSC_VLYNQ,
	LPSC_HPI,
	LPSC_DDR2,
	LPSC_AEMIF,
	LPSC_MMC_SD0,
	LPSC_ASP0,
	LPSC_GPIO,
	LPSC_IMCOP,
};

const size_t lpsc_emurstie_list_len = sizeof(lpsc_emurstie_list) /
	sizeof(lpsc_emurstie_list[0]);

/* DDR2 VTP Calibration */
void
ddr_vtp_calibration(void)
{
	int32_t cal_data;

	/* Enable VTP IO calibration bit (not started) */
	DDR->VTPIOCR = 0x0000001F | VTPIOCR_EN_MASK;

	/* Start VTP IO calibration */
	DDR->VTPIOCR |= VTPIOCR_RECAL_MASK;

	/* Wait for calibration to complete */
	waitloop(11*33);

	/* Enable access to DDRVTPR */
	SYSTEM->DDRVTPER = DDRVTPER_EN_MASK;

	cal_data = DDRVTPR & DDRVTPR_CAL_DATA_MASK; /* Read calibration data */

	/* Write calibration data to VTP Control register */
	DDR->VTPIOCR &= ~DDRVTPR_CAL_DATA_MASK;
	DDR->VTPIOCR |= cal_data;

	/* Disable VTP IO calibration bit */
	DDR->VTPIOCR &= ~VTPIOCR_EN_MASK;

	/* Disable access to DDRVTPR */
	SYSTEM->DDRVTPER = 0;
}
