/*
 * gpio.h - Gpio specific platform definitions
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _GPIO_H_
#define _GPIO_H_

#include "common.h"

#define GPIO(X) (X) /* 0 <= X <= (DAVINCI_N_GPIO - 1) */

int
gpio_direction_in(unsigned gpio);

int
gpio_direction_out(unsigned gpio, int initial_value);

void
gpio_set(unsigned gpio, int state);

#endif /* _GPIO_H_ */
