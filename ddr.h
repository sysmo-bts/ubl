/*
 * ddr.h - DDR devices parameters
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _DDDR_H_
#define _DDDR_H_

#include <stdint.h>

struct ddr_timing_infos_t {
	uint32_t SDBCR;
	uint32_t SDTIMR;
	uint32_t SDTIMR2;
	uint32_t SDRCR;
	uint8_t  READ_Latency;
};

#endif /* _DDDR_H_ */
