/*
 * crc.h -- CRC routines
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on dv-boot, original copyright follows:
 *   Copyright (c) 2007 Sergey Kubushin <ksi@koi8.net>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdint.h>
#include <string.h> /* For size_t */

#define CRC_TABLE_ELEMENTS 256
#define DAVINCI_CRC_POLY   0x04C11DB7

static uint32_t crc32_table[CRC_TABLE_ELEMENTS];

static uint32_t
reflect_num(uint32_t in_val, uint32_t num)
{
	uint32_t i;
	uint32_t out_val = 0x0;

	for (i = 1; i < (num + 1); i++) {
		out_val |= (uint32_t)(((in_val & 0x1)) << (num - i));
		in_val >>= 1;
	}

	return out_val;
}

/* Build a reflected CRC-32 table (for standard CRC-32 algorithm) */
void
crc32_dv_build_table(void)
{
	uint32_t i, j, crc_accum;

	for (i = 0; i < CRC_TABLE_ELEMENTS; i++) {
		crc_accum = reflect_num(i, 8) << (32 - 8);
		for (j = 0; j < 8; j++) {
			if ((crc_accum & 0x80000000) != 0x00000000)
				crc_accum = (crc_accum << 1) ^ DAVINCI_CRC_POLY;
			else
				crc_accum = (crc_accum << 1);

			crc32_table[i] = reflect_num(crc_accum, 32);
		}
	}
}

/* Compute CRC32 checksum */
uint32_t
crc32_dv_compute(uint8_t *data, size_t size)
{
	uint32_t crc32 = 0xFFFFFFFF;

	while (size-- > 0)
		crc32 = crc32_table[(crc32 ^ *data++) & 0xFF] ^ (crc32 >> 8);

	return crc32;
}
