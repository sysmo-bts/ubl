/*
 * util.c - miscellaneous functions
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdint.h>
#include <string.h>

#include "common.h"

#define MAXSTRLEN 256

/* Endian swap */
void
endian_data(uint32_t *data)
{
	uint32_t temp = *data;

	*data = ENDIAN_SWAP(temp);
}

void *
memcpy(void *dest, const void *src, size_t n)
{
	const uint8_t *src8 = src;
	uint8_t *dst8 = dest;

	for (; n > 0; n--) {
		*dst8 = *src8;
		dst8++;
		src8++;
	}

	return dest;
}

/* Get string length by finding null terminating char */
size_t
strlen(const char *s)
{
	int i = 0;

	while ((s[i] != 0) && (i < MAXSTRLEN))
		i++;

	if (i == MAXSTRLEN)
		return -1;
	else
		return i;
}

/* Simple wait loop */
void
waitloop(int32_t loopcnt)
{
	for (; loopcnt > 0; loopcnt--)
		asm("   NOP");
}

void
sleep_ms(int ms)
{
	for (; ms > 0; ms--)
		waitloop(20000);
}
