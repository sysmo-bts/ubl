#
# Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

CROSS_COMPILE=arm-none-linux-gnueabi-

.PHONY : clean check

CC=$(CROSS_COMPILE)gcc
LD=$(CROSS_COMPILE)ld

CFLAGS := -c -Os -Wall -mabi=aapcs-linux
LDFLAGS := -Map ubl.map -nostdlib

SOURCES := davinci.c uart.c uartboot.c ubl.c util.c gpio.c crc.c

# Boards setup
ifeq ($(BOARD),sysmobts_v2)
	PLATFORM   := DM644x
	FLASH_TYPE := FLASH_TYPE_NAND
endif
ifeq ($(BOARD),sysmobts_v1)
	PLATFORM   := DM644x
	FLASH_TYPE := FLASH_TYPE_NAND
endif
ifeq ($(BOARD),sffsdr)
	PLATFORM   := DM644x
	FLASH_TYPE := FLASH_TYPE_NAND
endif

# Generate a config.h file based on the board selected.
# Only update this file if the selected board is different.
OLDBOARD = $(shell cat config.h 2> /dev/null | grep "$(BOARD)")
ifneq ($(OLDBOARD),$(BOARD))
$(shell   echo "$(BOARD)" > config.h)
endif

CFLAGS += -D${PLATFORM} -D${FLASH_TYPE} -Dboard_$(BOARD) -DENABLE_BOOT_INTERRUPT -fno-strict-aliasing

# Processor type setup
# The Instruction and Data accesses are differentiated via accessing different
# memory map regions. The instruction region at 0x0000 and data region at
# 0x8000 (0x10000 for DM35x) map to the same physical TCM RAM.
ifeq ($(PLATFORM),DM644x)
	SOURCES += dm644x.c
	IRAM_SIZE  := 0x00004000
	DRAM_START := 0x00008000
	DRAM_SIZE  := 0x00004000
endif

LDFLAGS += --defsym __IRAM_SIZE=$(IRAM_SIZE) \
           --defsym __DRAM_START=$(DRAM_START) \
           --defsym __DRAM_SIZE=$(DRAM_SIZE) \
           -T ubl.lds

# NAND flash setup
ifeq ($(FLASH_TYPE),FLASH_TYPE_NAND)
	SOURCES += nandboot.c nand.c
endif
ifeq ($(FLASH_TYPE),FLASH_TYPE_NOR)
	SOURCES += norboot.c nor.c
endif

OBJECTS := $(patsubst %.c,%.o,$(SOURCES))
EXECUTABLE := ubl_$(BOARD).elf
BINARY := $(EXECUTABLE)

DEPS_DIR := .deps
# Creation of the dependencies directory
$(shell mkdir -p $(DEPS_DIR))

ifneq ($(MAKECMDGOALS),clean)
ifndef BOARD
all:
	@echo "You must select a board."
	@echo "List of supported boards: sysmobts_v2 sysmobts_v1 sffsdr"
	@echo "Example:"
	@echo "  make BOARD=sffsdr"; exit 1
else
ifndef PLATFORM
all:
	@echo "Invalid board"; exit 1
else
all: $(BINARY)
endif
endif
endif

# Including the dependency files (except during clean rules, so Make won't
# create them only to immediately remove them again). Each one of them will
# become a target in this Makefile (that is why the 'include' command must be
# placed after the 'all' target). If a dependency file is not found or is out
# of date, it is built or updated.
# If any have actually been changed, Make restarts with a clean state and
# reads all the dependency makefiles over again.
ifneq ($(MAKECMDGOALS),clean)
ifneq "$(SOURCES)" ""
ifdef BOARD
-include $(patsubst %.c,$(DEPS_DIR)/%.d,$(SOURCES))
endif
endif
endif

clean:
	-@rm -f -v *.o $(EXECUTABLE)
	-@rm -f -v *.map
	-@rm -f -v *~
	-@rm -f -v config.h
	-@rm -f -r $(DEPS_DIR)

check:
	-@checkpatch.pl --no-tree --file *.c *.h | more

$(EXECUTABLE): $(OBJECTS)
	$(LD) $(LDFLAGS) $(OBJECTS) -o $@

# Any source files depend on automatically generated config.h.
# This is necessary to recompile everything when we change boards.
*.o: config.h $(LINKERSCRIPT)

# The preprocessor of the compiler is used to generate a string representing
# the dependencies of the input file. This is done invoking the compiler with
# the -MM option (like -M but omit system header files). The purpose of the
# sed script is to add the name of the dependency file (.d) to the string
# returned by the preprocessor, like in the following example:
#   "main.o: main.c main.h" would become "main.o main.d: main.c main.h"
# The MAKE '$*' automatic variable represents the stem with which an implicit
# rule match. This would be 'main' in the above example.
#
# Use of the $(SHELL) function: Double quotes must be used to surround the
# command.
#
# In MAKE, using '$$' will produce a single dollar sign. When using only '$',
# MAKE tries to expand the variable following the dollar sign. Additionally,
# and for an obscure reason, '$1' must be preceded by a backslash on the
# command line. This is why '\$$1' is used in the command line of the shell to
# be seen as '$1' by the PERL script.
#
# The `-e' flag to the shell makes it exit immediately if the $(CC) command
# fails (exits with a nonzero status). Normally the shell exits with the
# status of the last command in the pipeline (sed in this case), so make would
# not notice a nonzero status from the compiler.
$(DEPS_DIR)/%.d: %.c
	@echo "Generating dependencies for $<"
	@$(SHELL) -ec '$(CC) -MM $(CPPFLAGS) $< | \
	  sed '\''s/\($*\)\.o[ :]*/\1.o $(DEPS_DIR)\/$*.d : /g'\'' > $@; \
	  [ -s $@ ] || rm -f $@'
