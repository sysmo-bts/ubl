/*
 * crc.h -- CRC definitions.
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef CRC_H
#define CRC_H 1

#include <stdint.h>
#include <string.h> /* For size_t, memcpy, memset */

/* Build a reflected CRC-32 table (for standard CRC-32 algorithm) */
void crc32_dv_build_table(void);

/* Compute non-standard CRC32 */
uint32_t
crc32_dv_compute(uint8_t *data, size_t size);

#endif /* CRC_H */
